use rocket_contrib::{json, json::JsonValue};
use mongodb;

use crate::models;

#[get("/api/v1/products/<name>", format = "application/json")]
pub fn get_one(name: String) -> JsonValue {
  let document = models::product::find_one(name.to_owned()).unwrap();
  let result = mongodb::from_bson::<models::product::ProductMongo>(mongodb::Bson::Document(document.unwrap()));

  match result {
    Ok(product) => {
      json!(product)
    },
    Err (_e) => {
      json!({
        "code": 400,
        "success": false,
        "data": {},
        "error": "An error has occured"
      })
    }
  }
}

#[get("/api/v1/products", format = "application/json")]
pub fn get_all() -> JsonValue {
  let product_cursor = models::product::find_all().unwrap();
  let result: Result<Vec<models::product::ProductMongo>, _> = product_cursor.unwrap().map(|doc| mongodb::from_bson::<models::product::ProductMongo>(mongodb::Bson::Document(doc.unwrap()))).collect();
  
  match result {
    Ok(products) => {
      json!(products)
    },
    Err (_e) => {
      json!({
        "code": 400,
        "success": false,
        "data": {},
        "error": "An error has occured"
      })
    }
  }
}