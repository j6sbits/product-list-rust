#![feature(proc_macro_hygiene, decl_macro)]
#[macro_use] extern crate rocket;
#[macro_use] extern crate serde_derive;
extern crate mongodb;
extern crate serde;
extern crate serde_json;

mod lib;
mod controllers;
mod models;

fn main() {
    rocket::ignite().mount("/", routes![
        controllers::product::get_one,
        controllers::product::get_all,
        ]).launch();
}
